#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
import urllib.parse

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to checkout a Git repository into a specified location.')
parser.add_argument('--repository', type=str, required=True)
parser.add_argument('--branch', type=str, default="master")
parser.add_argument('--into', type=str, required=True)
arguments = parser.parse_args()

# Make sure our base directory exists
if not os.path.exists( arguments.into ):
	# Then create it
	os.makedirs( arguments.into )

# Determine the path to where the repository will be checked out
repositoryUrl = urllib.parse.urlparse( arguments.repository )
repositoryName = os.path.basename( repositoryUrl.path )
if repositoryName.endswith(".git"):
	repositoryName = repositoryName[:-4]
localRepositoryPath = os.path.join( arguments.into, repositoryName )

# Let's see if something exists there already, and if so is it a valid Git repository?
if not os.path.exists( localRepositoryPath ) or not os.path.exists( os.path.join(localRepositoryPath, '.git') ):
	# Then we need to clone it...
	subprocess.check_call(["git", "clone", arguments.repository], cwd=arguments.into )

# Update the clone
subprocess.check_call( ["git", "fetch"], cwd=localRepositoryPath )

# Checkout the correct branch
subprocess.check_call( ["git", "checkout", arguments.branch], cwd=localRepositoryPath )

# Now that we know something exists, we can ask for it to be updated!
subprocess.check_call( ["git", "pull"], cwd=localRepositoryPath )

# If all of that worked fine, we can exit safely
sys.exit(0)
