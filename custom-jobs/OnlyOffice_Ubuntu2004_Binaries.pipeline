// Request a node to be allocated to us
node( "OnlyOfficeBuilder" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Fetching and Building') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Bring in the build tools used for ONLYOFFICE
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'build_tools/']],
				userRemoteConfigs: [[url: 'https://github.com/ONLYOFFICE/build_tools']]
			]

			// Start the actual build process
			//
			// Step 0: Transition ourselves to ~/ to ensure we don't hit any weird issues with path lengths or special characters
			// Step 1: Make sure 'python' is available - otherwise parts of the Gyp based build will fall over
			// Step 2: Patch scripts/build.py to ensure that Qt is actually found
			// Step 3: Fetch the source code for one of the sub-projects of OnlyOffice then patch it to remove the connections limit
			// Step 4: Fetch the source code of the 'desktop-sdk' and 'desktop-apps' modules as these aren't fetched when the 'desktop' flag isn't passed as a module
			// Step 5: Configure OnlyOffice to be built then build it
			// Step 6: Generate a tarball of the resulting binaries for later capture
			sh """
				mv build_tools/ ~/
				cd ~/

				mkdir ~/bin/
				ln -s /usr/bin/python2 ~/bin/python
				export PATH=\$HOME/bin:\$PATH

				sed -i '45i\\ \\ \\ \\ qt_dir = "/usr/"' build_tools/scripts/build.py

				git clone https://github.com/ONLYOFFICE/server.git server
				sed -i -e 's,^exports.LICENSE_CONNECTIONS = 20,exports.LICENSE_CONNECTIONS = 9999,' server/Common/sources/constants.js

				git clone https://github.com/ONLYOFFICE/desktop-sdk.git desktop-sdk
				git clone https://github.com/ONLYOFFICE/desktop-apps.git desktop-apps

				cd build_tools/
				python3 configure.py --module server --update 1 --qt-dir /usr/
				python3 make.py

				cd out/linux_64/onlyoffice/
				tar -czf \$WORKSPACE/onlyoffice-install.tar.gz documentserver/
			"""
		}

		// Capture the resulting ONLYOFFICE installation artifacts for deployment on Seges
		stage('Capturing Files') {
			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: 'onlyoffice-install.tar.gz', onlyIfSuccessful: true
		}
	}
}
}
